CPPFLAGS = -std=c++11 -g -W -Wall -Wno-sign-compare

all: main

main.o: main.cc util.hh

main: main.o
	c++ -o main main.o

clean:
	rm -rf *.o main

