#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <vector>
#include <list>
#include <string>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <stdio.h>
#include <cassert>

#include "util.hh"

#define EXPERT 6

using namespace std;

const set<string> adjectives = {
	"acute", "benign",
	"attenuated",
	"bilateral", "unilateral",
	"classic",
	"chronic",
	"congenital", "familial", "hereditary", "inherited",
	"infantile", "juvenile",
	"invasive",
	"primary",
	"progressive",
	"sporadic",
};

const set<string> nouns = {
	"deficiency",
	"disorder",
	"syndrome",
};

struct XMLTag {
	string text;
	string name;
	unordered_map<string, string> attributes;

	friend istream& operator>> (istream& is, XMLTag& t) {
		t.text.clear();
		t.attributes.clear();
		string str;
		getline(is, str, '<');
		t.text += str;
		getline(is, str, '>');
		if (str.size()) t.text += "<" + str + ">";
		if (str.back() == '/')
			str.resize(str.size() - 1);
		istringstream s(str);
		getline(s, t.name, ' ');
		for (string attr_name; getline(s, attr_name, '=');) {
			string attr_value;
			getline(s, attr_value, ' ');
			t.attributes.emplace(trim(attr_name), trim(attr_value));
		}
		return is;
	}

	friend ostream& operator<< (ostream& os, XMLTag const& t) {
		os << t.text;
		return os;
	}

};

struct Annotation {
	int document_id;
	int id;
	int annotator_id;
	int offset;
	int length;
	double weight;
	string text;

	bool operator== (Annotation const& a) const {
		return document_id == a.document_id && offset == a.offset && length == a.length;
	}

	bool operator< (Annotation const& a) const {
		if (document_id < a.document_id)
			return true;
		if (document_id > a.document_id)
			return false;
		if (offset < a.offset)
			return true;
		if (offset > a.offset)
			return false;
		return length < a.length;
	}

	bool intersects (Annotation const& a) const {
		if (document_id != a.document_id)
			return false;
		if (offset <= a.offset && offset + length > a.offset)
			return true;
		if (offset > a.offset && a.offset + a.length > offset)
			return true;
		return false;
	}

	friend ostream& operator<< (ostream& os, Annotation const& a) {
		os << a.document_id << ": " << a.offset << " " << a.length << " " << a.text << endl;
		return os;
	}
};

map<int, double> weights;

map<Annotation, set<int>> voters_for_annotation;
map<Annotation, set<int>> voters_against_annotation;

double pow_base = 50000;

struct Document {
	int id;
	string source;
	set<int> annotators;
	string title;
	int title_offset;
	string abstract;
	int abstract_offset;
	vector<Annotation> annotations;
	vector<Annotation> chosen_annotations;
	set<Annotation> annotation_set;
	set<Annotation> approved_annotations;
	set<Annotation> disproved_annotations;

	set<Annotation> get_annotations(int id) const {
		set<Annotation> s;
		for (auto const& a : annotations) {
			if (a.annotator_id == id)
				s.insert(a);
		}
		return s;
	}

	void construct_annotation_set () {
		annotation_set.clear();
		for (auto const& a : annotations)
			annotation_set.insert(a);
	}

	void set_annotation_voters () {
		for (int i = 0; i < annotations.size();) {
			Annotation const& a = annotations[i];
			set<int> approve;
			approve.insert(a.annotator_id);
			voters_for_annotation[a].insert(a.annotator_id);
			int j = i + 1;
			while (j < annotations.size() && a == annotations[j]) {
				approve.insert(annotations[j].annotator_id);
				voters_for_annotation[a].insert(annotations[j].annotator_id);
				j++;
			}
			set_difference(annotators.cbegin(), annotators.cend(), approve.cbegin(), approve.cend(),
					inserter(voters_against_annotation[a], voters_against_annotation[a].begin()));
			i = j;
		}
	}

	void extend_annotations () {
		for (int i = 0; i < annotations.size();) {
			int j = i + 1;
			while (j < annotations.size() && annotations[i] == annotations[j]) {
				j++;
			}
			if (j == annotations.size())
				break;
			if (annotations[i].intersects(annotations[j])) {
				bool something_changed = false;
				if (annotations[i].offset < annotations[j].offset) {
					string prefix = annotations[i].text.substr(0, annotations[j].offset - annotations[i].offset);
					istringstream s(prefix);
					for (string word; getline(s, word, ' ');) {
						if (!word.size())
							continue;
						transform(word.begin(), word.end(), word.begin(), ::tolower);
						if (!adjectives.count(word)) {
							goto try_next;
						}
					}
					for (int k = j; k < annotations.size() && annotations[j] == annotations[k]; k++) {
						annotations[k].text = prefix + annotations[k].text;
						annotations[k].offset = annotations[i].offset;
						annotations[k].length += prefix.size();
					}
					something_changed = true;
				}
			try_next:
				if (annotations[i].offset + annotations[i].length < annotations[j].offset + annotations[j].length) {
					string postfix = annotations[j].text.substr(annotations[i].offset + annotations[i].length - annotations[j].offset, 
							annotations[j].offset + annotations[j].length - annotations[i].offset - annotations[i].length);
					string word = postfix;
					word = trim(word);
					transform(word.begin(), word.end(), word.begin(), ::tolower);
					if (nouns.count(word)) {
						for (int k = i; k < j; k++) {
							annotations[k].text = annotations[k].text + postfix;
							annotations[k].length += postfix.size();
						}
						something_changed = true;
					}
				}
				if (annotations[j].offset + annotations[j].length < annotations[i].offset + annotations[i].length) {
					string postfix = annotations[i].text.substr(annotations[j].offset + annotations[j].length - annotations[i].offset, 
							annotations[i].offset + annotations[i].length - annotations[j].offset - annotations[j].length);
					string word = postfix;
					word = trim(word);
					transform(word.begin(), word.end(), word.begin(), ::tolower);
					if (nouns.count(word)) {
						int k = j + 1;
						while (k < annotations.size() && annotations[j] == annotations[k]) {
							k++;
						}
						for (int l = j; l < k; l++) {
							annotations[l].text = annotations[l].text + postfix;
							annotations[l].length += postfix.size();
						}
						something_changed = true;
					}
				}
				if (!something_changed)
					i = j;
			} else {
				i = j;
			}
		}
	}

	void extend_chosen_annotations () {
		sort(chosen_annotations.begin(), chosen_annotations.end());
		for (int i = 0; i < chosen_annotations.size();) {
			string text = chosen_annotations[i].offset < abstract_offset ? title : abstract;
			int text_offset = chosen_annotations[i].offset < abstract_offset ? title_offset : abstract_offset;
			bool something_changed = false;
			int ann_off = chosen_annotations[i].offset - text_offset;
			int j = text.find(chosen_annotations[i].text, ann_off);
			if (j == string::npos) {
				cout << "Can't find annotation in text: " << text << endl;
				PR(chosen_annotations[i]);
				i++;
				continue;
			}
			ann_off = j;
			j--;
			string word;
			for (; j >= 0 && !(text[j] == ' ' && word.size()); j--) {
				word = text.substr(j, ann_off - j);
			}
			string w = word;
			w = trim(w);
			transform(w.begin(), w.end(), w.begin(), ::tolower);
			if (adjectives.count(w)) {
				chosen_annotations[i].text = word + chosen_annotations[i].text;
				chosen_annotations[i].offset -= word.size();
				chosen_annotations[i].length += word.size();
				something_changed = true;
			}
			ann_off = ann_off + chosen_annotations[i].text.size();
			j = ann_off;
			word = "";
			for (; j < text.size() && !(text[j] == ' ' && word.size()); j++) {
				word = text.substr(ann_off, j - ann_off + 1);
			}
			w = word;
			w = trim(w);
			transform(w.begin(), w.end(), w.begin(), ::tolower);
			if (nouns.count(w)) {
				chosen_annotations[i].text += word;
				chosen_annotations[i].length += word.size();
				something_changed = true;
			}
			if (!something_changed)
				i++;
		}
	}

	void set_approved_annotations (int control) {
		approved_annotations.clear();
		disproved_annotations.clear();
		for (auto& a : annotation_set) {
			if (voters_for_annotation[a].count(EXPERT)) {
				approved_annotations.insert(a);
				continue;
			}
			if (voters_against_annotation[a].count(EXPERT)) {
				disproved_annotations.insert(a);
				continue;
			}
			vector<double> approve_wheights(voters_for_annotation[a].size());
			transform(voters_for_annotation[a].begin(), voters_for_annotation[a].end(), approve_wheights.begin(),
					[](int i) { return pow(pow_base, weights[i]); });
			sort(approve_wheights.begin(), approve_wheights.end());
			vector<double> disprove_wheights(voters_against_annotation[a].size());
			transform(voters_against_annotation[a].begin(), voters_against_annotation[a].end(), disprove_wheights.begin(),
					[](int i) { return pow(pow_base, weights[i]); });
			sort(disprove_wheights.begin(), disprove_wheights.end());
			double approve, disprove;
			//if (control > 1) {
			//	int sz = min((size_t)3, min(approve_wheights.size(), disprove_wheights.size()));
			//	approve = accumulate(approve_wheights.rbegin(), approve_wheights.rbegin() + sz, 0.0);
			//	disprove = accumulate(disprove_wheights.rbegin(), disprove_wheights.rbegin() + sz, 0.0);
			//} else {
				approve = accumulate(approve_wheights.rbegin(), approve_wheights.rend(), 0.0);
				disprove = accumulate(disprove_wheights.rbegin(), disprove_wheights.rend(), 0.0);
			//}
			if (control > 1) {
				if (approve / approve_wheights.size() > disprove / disprove_wheights.size())
				//if (approve_wheights.size() && approve_wheights.back() > pow(pow_base, 0.6))
					approved_annotations.insert(a);
				else
					disproved_annotations.insert(a);
			} else {
				//if (approve / approve_wheights.size() > 0.5)
				if (approve >= disprove)
					approved_annotations.insert(a);
				else
					disproved_annotations.insert(a);
			}
		}
	}

#if 0
	void choose_annotaions () {
		for (int i = 0; i < annotations.size();) {
			Annotation const& a = annotations[i];
			int j = i + 1;
			while (j < annotations.size() && a == annotations[j]) {
				j++;
			}
			if (disproved_annotations.count(a)) {
				i = j;
				continue;
			}
			if (voters_for_annotation[a].count(EXPERT)) {
					chosen_annotations.push_back(a);
			}
			else if (j == annotations.size() || a.offset + a.length <= annotations[j].offset) {
				chosen_annotations.push_back(a);
			} else {
				int k = j + 1;
				while (k < annotations.size() && annotations[j] == annotations[k]) {
					k++;
				}
				double approve_i = accumulate(voters_for_annotation[a].begin(), voters_for_annotation[a].end(), 0.0,
						[](double x, int i) {
							return x + weights[i];
						});
				double approve_j = accumulate(voters_for_annotation[annotations[j]].begin(), voters_for_annotation[annotations[j]].end(), 0.0,
						[](double x, int i) {
							return x + weights[i];
						});
				if (disproved_annotations.count(annotations[j]) || approve_i > approve_j) {
					PR2("Choose ", a);
					PR(annotations[j]);
					annotations.erase(annotations.begin() + j, annotations.begin() + k);
					j = i;
				} else {
						PR(a);
						PR2("Choose ", annotations[j]);
				}
#if 0
				set<int> disapprove;
				set_difference(annotators.cbegin(), annotators.cend(), approve.cbegin(), approve.cend(), inserter(disapprove, disapprove.begin()));

				double unweight = 0;
				for (int a_id : disapprove)
					unweight += weights[a_id];
				if (weight - unweight > 0)
					chosen_annotations.push_back(annotations[i]);
				//if (approve.size() >= annotators.size() / 2.0)
				//	chosen_annotations.push_back(annotations[i]);
#endif
			}
			i = j;
		}
		//PR(annotations);
		//PR(chosen_annotations);
	}

#else

	void choose_annotaions() {
		for (int i = 0; i < annotations.size();) {
			set<int> approve;
			approve.insert(annotations[i].annotator_id);
			double weight = weights[annotations[i].annotator_id];
			int j = i + 1;
			while (j < annotations.size() && annotations[i] == annotations[j]) {
				approve.insert(annotations[j].annotator_id);
				weight += weights[annotations[j].annotator_id];
				j++;
			}
			if (approve.count(EXPERT)) {
				chosen_annotations.push_back(annotations[i]);
			} else if (!annotators.count(EXPERT)) {
				set<int> disapprove;
				set_difference(annotators.cbegin(), annotators.cend(), approve.cbegin(), approve.cend(), inserter(disapprove, disapprove.begin()));

				double unweight = 0;
				for (int a_id : disapprove)
					unweight += weights[a_id];
				if (weight - unweight > 0)
					chosen_annotations.push_back(annotations[i]);
			}
			i = j;
		}
	}
#endif

	void choose_extra (int control) {
		vector<Annotation> vec;
		bool fresh_start = !chosen_annotations.size();
		for (auto const& a : annotation_set) {
			if (voters_for_annotation[a].count(EXPERT)) {
				if (fresh_start)
					chosen_annotations.push_back(a);
			} else if (!voters_against_annotation[a].count(EXPERT)) {
				vec.push_back(a);
			}
		}
		if (control > 10) { // never
			//for (auto& a : vec) {
			//	a.weight = accumulate(voters_for_annotation[a].begin(), voters_for_annotation[a].end(), -1,
			//					[](double x, int i) {
			//						return weights[i] < 0.6 ? x : pow(pow_base, weights[i]);
			//					});
			//}
			for (auto& a : vec) {
				a.weight = accumulate(voters_for_annotation[a].begin(), voters_for_annotation[a].end(), 0.0,
								[](double x, int i) {
									return x + pow(pow_base, weights[i]);
								}) / voters_for_annotation[a].size() -
						   accumulate(voters_against_annotation[a].begin(), voters_against_annotation[a].end(), 0.0,
								[](double x, int i) {
									return x + pow(pow_base, weights[i]);
								}) / voters_against_annotation[a].size();
			}
		} else {
			for (auto& a : vec) {
				a.weight = accumulate(voters_for_annotation[a].begin(), voters_for_annotation[a].end(), 0.0,
								[](double x, int i) {
									return x + pow(pow_base, weights[i]);
								}) -
						   accumulate(voters_against_annotation[a].begin(), voters_against_annotation[a].end(), 0.0,
								[](double x, int i) {
									return x + pow(pow_base, weights[i]);
								});
			}
		}
		sort(vec.begin(), vec.end(), [](Annotation const& a, Annotation const& b) { return a.weight > b.weight; });
		for (auto const& a : vec) {
			if (a.weight < 0)
				break;
			for (auto const& b : chosen_annotations) {
				if (a.intersects(b))
					goto next_a;
			}
			chosen_annotations.push_back(a);
		next_a: {}
		}
	}

	friend ostream& operator<< (ostream& os, Document const& d) {
		os << d.id << " -- " << d.annotators;
		os << d.title_offset << ": " << d.title << endl;
		os << d.abstract_offset << ": " << d.abstract << endl;
		return os;
	}
};

unordered_map<int, Document> documents;

void parse(istream& is) {
	//for (int i = 0; i < 30; i++) {
	while (is) {
		XMLTag tag;
		is >> tag;
		if (tag.name == "document") {
			is >> tag;
			assert(tag.name == "id");
			int id;
			is >> id;
			Document& d = documents[id];
			d.id = id;
			string* str;
			int* str_off;
			is >> tag; // </id>
			for (; tag.name != "/document"; is >> tag) {
				if (tag.name == "infon" && tag.attributes["key"] == "type") {
					string type;
					getline(is, type, '<');
					is.unget();
					if (type == "title") {
						str = &d.title;
						str_off = &d.title_offset;
					}
					else if (type == "abstract") {
						str = &d.abstract;
						str_off = &d.abstract_offset;
					}
					else {
						PR(type);
					}
					is >> tag; // </infon>
				}
				else if (tag.name == "offset") {
					is >> *str_off;
					is >> tag; // </offset>
				}
				else if (tag.name == "text") {
					getline(is, *str, '<');
					is.unget();
					is >> tag; // </text>
				}
				else if (tag.name == "annotation") {
					Annotation a;
					a.document_id = d.id;
					a.id = stoi(tag.attributes["id"]);
					is >> tag;
					for (; tag.name != "/annotation"; is >> tag) {
						if (tag.name == "infon" && tag.attributes["key"] == "annotator_id") {
							is >> a.annotator_id;
							is >> tag; // </infon>
							d.annotators.insert(a.annotator_id);
						}
						else if (tag.name == "infon" && tag.attributes["key"] == "type") {
							string type;
							getline(is, type, '<');
							is.unget();
							if (type != "Disease") {
								PR(type);
							}
							is >> tag; // </infon>
						}
						else if (tag.name == "location") {
							a.offset = stoi(tag.attributes["offset"]);
							a.length = stoi(tag.attributes["length"]);
						}
						else if (tag.name == "text") {
							getline(is, a.text, '<');
							is.unget();
							is >> tag; // </text>
						}
					}
					d.annotations.push_back(a);
				}
			}
			//PR(d.annotations);
			//documents.push_back(d);
		}
	}

	//PR(documents);
}

void output_documents (ostream& os) {
	for (auto const& d : documents) {
		os << "<document><id>" << d.first << "</id>";
		os << "<infon key=\"n_annotators\">1</infon><infon key=\"annotator_ids\">[1]";
		os << "</infon><passage><infon key=\"type\">title</infon><offset>" << d.second.title_offset << "</offset>";
		os << "<text>" << d.second.title << "</text>" << endl;
		for (auto const& a : d.second.chosen_annotations) {
			if (a.offset >= d.second.abstract_offset)
				continue;
			os << "<annotation id=\"" << a.id << "\"><infon key=\"annotator_id\">1</infon>";
			os << "<infon key=\"type\">Disease</infon>";
			os << "<location offset=\"" << a.offset << "\" length=\"" << a.length << "\" /><text>" << a.text << "</text></annotation>" << endl;
		}
		os << "</passage><passage><infon key=\"type\">abstract</infon><offset>" << d.second.abstract_offset << "</offset>";
		os << "<text>" << d.second.abstract << "</text>" << endl;
		for (auto const& a : d.second.chosen_annotations) {
			if (a.offset < d.second.abstract_offset)
				continue;
			os << "<annotation id=\"" << a.id << "\"><infon key=\"annotator_id\">1</infon>";
			os << "<infon key=\"type\">Disease</infon>";
			os << "<location offset=\"" << a.offset << "\" length=\"" << a.length << "\" /><text>" << a.text << "</text></annotation>" << endl;
		}
		os << "</passage></document>";
	}
}

inline double mcc (double tps, double fps, double tns, double fns) {
	return (tps*tns - fps*fns) / sqrt((tps + fps)*(tps + fns)*(tns + fps)*(tns + fns));
}

inline double f1_score (double tps, double fps, double fns) {
	double precision = tps / (tps + fps);
	double recall = tps / (tps + fns);
	return tps > 0 ? 2 * precision * recall / (precision + recall) : 0;
}

inline void trim_annotations(double threshold) {
	for (auto& d : documents) {
		for (auto it_ann = d.second.annotators.begin(); it_ann != d.second.annotators.end();) {
			int ann = *it_ann;
			if (weights[ann] < threshold) {
				it_ann = d.second.annotators.erase(it_ann);
				for (auto it_a = d.second.annotations.begin(); it_a != d.second.annotations.end();) {
					if (it_a->annotator_id == ann) {
						it_a = d.second.annotations.erase(it_a);
					} else {
						++it_a;
					}
				}
				d.second.construct_annotation_set();
			} else {
				++it_ann;
			}
		}
	}

	for (auto it = weights.begin(); it != weights.end();) {
		if (it->second < threshold) {
			for (auto& a : voters_for_annotation)
				a.second.erase(it->first);
			for (auto& a : voters_against_annotation)
				a.second.erase(it->first);
			it = weights.erase(it);
		} else {
			++it;
		}
	}
}

int main ()  {
	ifstream is("../banner/training_data/expert1_bioc.xml");
	parse(is);
	is.close();
	is.open("../banner/training_data/mturk1_bioc.xml");
	parse(is);
	is.close();
	is.open("../banner/training_data/mturk2_bioc.xml");
	parse(is);
	is.close();

	for (auto& d : documents) {
		sort(d.second.annotations.begin(), d.second.annotations.end());
		//d.second.extend_annotations();
		d.second.set_annotation_voters();
		d.second.construct_annotation_set();
	}

	//for (auto const& a : voters_for_annotation) {
	//	PR(a.first);
	//	PR(a.second);
	//	PR(voters_against_annotation[a.first]);
	//}

	auto documents_copy = documents;

	for (int hyper_count = 0; hyper_count <= 2; hyper_count++) {
	//for (int hyper_count = 2; hyper_count >= 0; hyper_count--) {

		PR(hyper_count);

		int count_down = 20;
		double threshold = 0;
		pow_base = 50000;

		switch (hyper_count) {
			case 1:
				count_down = 70;
				threshold = 0.5;
				pow_base = 16000;
				break;
			case 2:
				pow_base = 4;
				break;
			default: ;
		} 

		map<int, double> tps;
		map<int, double> fps;
		map<int, double> tns;
		map<int, double> fns;

		map<int, vector<double>> score;

		for (auto const& d : documents) {
			set<Annotation> exp = d.second.get_annotations(EXPERT);
			if (!exp.size())
				continue;
			for (auto const& i : d.second.annotators) {
				set<Annotation> turk = d.second.get_annotations(i);
				set<Annotation> negatives;
				set_difference(d.second.annotation_set.cbegin(), d.second.annotation_set.cend(), exp.cbegin(), exp.cend(),
						inserter(negatives, negatives.begin()));
				set<Annotation> tp;
				set<Annotation> tn;
				set<Annotation> fp;
				set<Annotation> fn;
				set_intersection(exp.cbegin(), exp.cend(), turk.cbegin(), turk.cend(), inserter(tp, tp.begin()));
				set_difference(negatives.cbegin(), negatives.cend(), turk.cbegin(), turk.cend(), inserter(tn, tn.begin()));
				set_intersection(negatives.cbegin(), negatives.cend(), turk.cbegin(), turk.cend(), inserter(fp, fp.begin()));
				set_difference(exp.cbegin(), exp.cend(), turk.cbegin(), turk.cend(), inserter(fn, fn.begin()));

				tps[i] += tp.size();
				fps[i] += fp.size();
				tns[i] += tn.size();
				fns[i] += fn.size();

				score[i].push_back(f1_score(tp.size(), fp.size(), fn.size()));
			}
		}

		if (hyper_count > 0) {
			for (auto& i : tps) {
				//PR5(i.first, tps[i.first], fps[i.first], tns[i.first], fns[i.first]);
				weights[i.first] = mcc(tps[i.first], fps[i.first], tns[i.first], fns[i.first]);
			}
		} else {
			for (auto& i : score) {
				weights[i.first] = accumulate(i.second.begin(), i.second.end(), 0.0) / i.second.size();
			}
		}

		PR(weights);

		trim_annotations(threshold);

		PR(weights);

		for (auto& d : documents) {
			d.second.set_approved_annotations(hyper_count);
		}

		while (count_down--) {
			tps.clear();
			fps.clear();
			tns.clear();
			fns.clear();

			score.clear();

			for (auto const& d : documents) {
				auto positives = d.second.approved_annotations;
				auto negatives = d.second.disproved_annotations;
				for (auto const& i : d.second.annotators) {
					auto obs = d.second.get_annotations(i);
					set<Annotation> tp;
					set<Annotation> tn;
					set<Annotation> fp;
					set<Annotation> fn;
					set_intersection(positives.cbegin(), positives.cend(), obs.cbegin(), obs.cend(), inserter(tp, tp.begin()));
					set_difference(negatives.cbegin(), negatives.cend(), obs.cbegin(), obs.cend(), inserter(tn, tn.begin()));
					set_intersection(negatives.cbegin(), negatives.cend(), obs.cbegin(), obs.cend(), inserter(fp, fp.begin()));
					set_difference(positives.cbegin(), positives.cend(), obs.cbegin(), obs.cend(), inserter(fn, fn.begin()));

					tps[i] += tp.size();
					fps[i] += fp.size();
					tns[i] += tn.size();
					fns[i] += fn.size();

					score[i].push_back(f1_score(tp.size(), fp.size(), fn.size()));
				}
			}

			bool need_annotation_trimming = false;

			if (hyper_count > 0) {
				for (auto& w : weights) {
					//PR5(w.first, tps[w.first], fps[w.first], tns[w.first], fns[w.first]);
					w.second = mcc(tps[w.first], fps[w.first], tns[w.first], fns[w.first]);
					if (w.second < threshold)
						need_annotation_trimming = true;
				}
			} else {
				for (auto& i : score) {
					weights[i.first] = accumulate(i.second.begin(), i.second.end(), 0.0) / i.second.size();
				}
			}

			PR(weights);

			if (need_annotation_trimming) {
				trim_annotations(threshold);
				PR(weights);
			}

			for (auto& d : documents) {
				d.second.set_approved_annotations(hyper_count);
			}
		}

		//for (auto& d : documents)
		//	d.second.choose_annotaions();

		for (auto& d : documents)
			d.second.choose_extra(hyper_count);

		for (auto& d : documents)
			documents_copy[d.first].chosen_annotations = d.second.chosen_annotations;

		documents = documents_copy;
	}

	for (auto const& ann : voters_for_annotation) {
		Document& d = documents[ann.first.document_id];
		for (auto const& a : d.chosen_annotations) {
			if (ann.first.text == a.text) {
				for (auto const& b : d.chosen_annotations) {
					if (ann.first.intersects(b))
						goto next_ann;
				}
				d.chosen_annotations.push_back(ann.first);
				goto next_ann;
			}
		}
	next_ann: {}
	}

	for (auto& d : documents)
		d.second.extend_chosen_annotations();

	ofstream os("../banner/training_data/ncbi_body.xml");
	output_documents(os);
	os.close();

	os.open("../banner/training_data/ann");
	map<int, Document> sorted_docs;
	for (auto const& d : documents)
		sorted_docs[d.first] = d.second;
	for (auto& d : sorted_docs) {
		sort(d.second.chosen_annotations.begin(), d.second.chosen_annotations.end());
		for (auto const& a : d.second.chosen_annotations)
			os << a;
	}
	os.close();

	return 0;
}

// vim: sw=4 ts=4
