#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

// -- Strings --

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        ltrim(rtrim(s));
	if (s.size() >= 2 &&
	    ((s.front() == '"' && s.back() == '"') ||
	     (s.front() == '\'' && s.back() == '\''))) {
		s.erase(s.begin());
		s.erase(s.end() - 1);
	}
	return s;
}

// -- Printing --

template<class T1, class T2>
std::ostream& operator<<(std::ostream& os, const std::pair<T1, T2>& p)
{
	os << "{" << p.first << ", " << p.second << "}";
	return os;
}

template<class T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& s)
{
	os << "{";
	for (auto &e : s) os << e << ", ";
	os << "}" << std::endl;
	return os;
}

template<class K>
std::ostream& operator<<(std::ostream& os, const std::unordered_set<K>& s)
{
	os << "{";
	for (auto &k : s) os << k << ", ";
	os << "}" << std::endl;
	return os;
}

template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
	os << "{";
	for (auto &e : v) os << e << ", ";
	os << "}" << std::endl;
	return os;
}

template<class K, class V>
std::ostream& operator<<(std::ostream& os, const std::map<K, V>& m)
{
	os << "{";
	for (auto &kv : m) os << "{" << kv.first << ", " << kv.second << "}, ";
	os << "}" << std::endl;
	return os;
}

template<class K, class V>
std::ostream& operator<<(std::ostream& os, const std::unordered_map<K, V>& m)
{
	os << "{";
	for (auto &kv : m) os << "{" << kv.first << ", " << kv.second << "}, ";
	os << "}" << std::endl;
	return os;
}

#define PR(x1) cout << #x1 ": " << x1 << endl;
#define PR2(x1, x2) cout << #x1 ": " << x1 << ", " << #x2 ": " << x2 << endl;
#define PR3(x1, x2, x3) \
	cout << #x1 ": " << x1 << ", " << #x2 ": " << x2 \
			       << ", " << #x3 ": " << x3 << endl;
#define PR4(x1, x2, x3, x4) \
	cout << #x1 ": " << x1 << ", " << #x2 ": " << x2 \
			       << ", " << #x3 ": " << x3 \
			       << ", " << #x4 ": " << x4 << endl;
#define PR5(x1, x2, x3, x4, x5) \
	cout << #x1 ": " << x1 << ", " << #x2 ": " << x2 \
			       << ", " << #x3 ": " << x3 \
			       << ", " << #x4 ": " << x4 \
			       << ", " << #x5 ": " << x5 << endl;
#define PR6(x1, x2, x3, x4, x5, x6) \
	cout << #x1 ": " << x1 << ", " << #x2 ": " << x2 \
			       << ", " << #x3 ": " << x3 \
			       << ", " << #x4 ": " << x4 \
			       << ", " << #x5 ": " << x5 \
			       << ", " << #x6 ": " << x6 << endl;

